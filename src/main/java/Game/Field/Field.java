package Game.Field;

import Game.Player.Player;

import java.util.ArrayList;

public class Field {
    final char BLACK_CELL = '□';
    final char WHITE_CELL = '■';
    Cell[][] field;

    public Field(int rows, int columns) {
        field = new Cell[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if(j < columns/2) {
                    field[i][j] = new Cell(0, "black", (i == rows / 2 && j == 0 ? '⛄' : BLACK_CELL));
                }else{
                    field[i][j] = new Cell(0, "white", (i == rows / 2 && j == columns-1? '⛲' : WHITE_CELL));
                }
            }
        }
        Randomizer.randomizerForFields(field);
    }

    public void printField(){
        for (int i = -1; i < field.length; i++) {
            for (int j = -1; j < field[0].length; j++) {
                if(j == -1){
                    if(i == -1){
                        System.out.print("   ");
                        continue;
                    }
                    System.out.print("[" + i + "]\t");
                    continue;
                }
                if(i == -1){
                    System.out.print("[" + j + "]\t");
                    continue;
                }
                System.out.print(field[i][j].getSymbol() + "\t");
            }
            System.out.println();
        }
    }

    public int shoot(Player player, int row, int column){
        try {
            if (field[row][column].checkIfYouSootYourField(player)) {
                player.addPoint(field[row][column].getPoints());
                field[row][column].setPoints(0);
                char symbol;
                if (player.getColor().equals("white")) {
                    symbol = WHITE_CELL;
                } else {
                    symbol = BLACK_CELL;
                }
                field[row][column].setSymbol(symbol);
            }
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("You shooted out of the field, DON'T DO THAT");
        }
        return 0;
    }
}
