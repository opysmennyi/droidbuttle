package Game.Field;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Randomizer {

    public static Cell[][] randomizerForFields (Cell[][] cellArray) {
        List<Integer> listOfPoints = setPointList();

        Random r = new Random();
        for (int i = 0; i < cellArray.length; i++) {
            for (int j = 0; j <  cellArray[0].length/2; j++) {
                int randomPointIndex = getRandomValue(r, listOfPoints);
                if(!(i==2 && j==0)) {
                    final Integer points = listOfPoints.get(randomPointIndex);
                    cellArray[i][j].setPoints(points);
                    listOfPoints.remove(randomPointIndex);
                }
            }
        }
        listOfPoints = setPointList();
        for (int i = 0; i < cellArray.length; i++) {
            for (int j =  cellArray[0].length/2; j <  cellArray[i].length; j++) {
                int randomPointIndex = getRandomValue(r, listOfPoints);
                if( !(i==2 && j==7)) {
                    cellArray[i][j].setPoints(listOfPoints.get(randomPointIndex));
                    listOfPoints.remove(randomPointIndex);
                }
            }
        }
        return cellArray;
    }

    private static List<Integer> setPointList() {
        List<Integer> listOfPoints = new ArrayList<>();
        for (int i = 0; i <5 ; i++) {
            listOfPoints.add(5);
        }
        for (int i = 0; i <10 ; i++) {
            listOfPoints.add(2);
        }
        for (int i = 0; i <4 ; i++) {
            listOfPoints.add(1);
        }
        return listOfPoints;
    }

    private static int getRandomValue(Random random, List<Integer> intArray){
        int min = 0;
        int max = intArray.size()-1;
        return random.nextInt((max) + 1);
    }
}