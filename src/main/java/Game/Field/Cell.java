package Game.Field;

import Game.Player.Player;

public class Cell {


    private int points;
    private String color;
    private char symbol;

    public Cell() {
    }

    public Cell(int points, String color, char symbol) {
        this.points = points;
        this.color = color;
        this.symbol = symbol;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char simbol) {
        this.symbol = simbol;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }


    public boolean checkIfYouSootYourField(Player player) {
        if (!this.getColor().equals(player.getColor()) && points != 0) {
            System.out.println("Well done");
            return true;
        } else if (!this.getColor().equals(player.getColor()) && points == 0) {
            System.out.println("Someone already shooted here, pick another target!");
            return false;
        } else if (this.getColor().equals(player.getColor())) {
            System.out.println("Don't shoot yourself!!!");
            return false;
        }
        return true;
    }
}

