package Game.Player;

public class Player {
    public String getColor() {
        return color;
    }

    String color;

    public int getNumberofpoint() {

        return numberofpoint;
    }

    public void addPoint(int numberofpoint) {

        this.numberofpoint += numberofpoint;
    }

    int numberofpoint;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }

    String name;


    public Player(String color, int numberofpoint, String name) {
        this.color = color;
        this.numberofpoint = numberofpoint;
        this.name = name;
    }
}
