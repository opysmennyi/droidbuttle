import Game.Field.Field;
import Game.Player.Player;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String check = "1";
        do {
            boolean step = true;
            System.out.println("Player 1 please enter your name");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String playername1 = null;
            String playername2 = null;
            try {
                playername1 = reader.readLine();
                System.out.println("Player 2 please enter your name");
                playername2 = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
            Player player1 = new Player("black", 0, playername1);
            Player player2 = new Player("white", 0, playername2);
            Field field = new Field(5, 8);
            field.printField();
            while ((player1.getNumberofpoint() < 30 && player2.getNumberofpoint() < 30) &&
                    !check.equalsIgnoreCase("q")) {
                try {
                    doStep(field, step ? player1 : player2);
                    step = !step;
                } catch (IOException e) {
                    System.out.println("Incorrect data");
                }
                System.out.println("For exit write 'q'");
                try {
                    check = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }

            System.out.println("Congratulations. Winner is " +
                    (player1.getNumberofpoint() > player2.getNumberofpoint() ?
                            player1.getName() : player2.getName()));

            System.out.println("Would you like to play again?(1 - yes, 0 - no)");
            try {
                check = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }while (check.equals("1"));

    }

    private static void doStep(Field field, Player player) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(player.getName() + " choose row and column where you want to make a shot(Example: row column)");
        String place = reader.readLine();
        char r = place.charAt(0);
        char c = place.charAt(2);
        if(Character.isDigit(r)&& Character.isDigit(c)) {

            int row = place.charAt(0) - '0';
            int column = place.charAt(2) - '0';
            field.shoot(player, row, column);
            field.printField();

        }else {
            System.out.println("Please change the coordinates, the input was was wrong!");
        }
        System.out.println(player.getName() + " you have got " + player.getNumberofpoint() + " points\n\n\n");

    }
}
